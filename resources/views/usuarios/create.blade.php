@php
    $empresa = DB::table('empresa_users')->where('id_user','=',Auth::user()->id)->select('id_empresa')->get();
    if ($empresa[0]->id_empresa != 1) {
        $sitio = DB::table('empresas')->where('id','=',$empresa[0]->id_empresa)->select('site')->get();
        header("Status: 200 Redireccion Programada");
        //dd("Location: ".$sitio[0]->site);
        header("Location: ".$sitio[0]->site);
        exit;
    }
@endphp
<x-app-layout>
	<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Clientes') }}
        </h2>
        <div class="card col-md-6">
        	<div class="card-body">
        		<div class="panel">
        			<form method="POST" action="/Clientes/create">
        				@csrf
	                    <div class="form-inline d-flex justify-content-between">
	                        <label>Nombre: </label><input type="text" id="nombre" name="nombre" placeholder="nombre">
	                    </div>
	                    <div class="form-inline d-flex justify-content-between">
	                        <label>RUT: </label><input type="text" id="rut" name="rut" placeholder="xx.xxx.xxx-x">
	                    </div>
	                    <div class="form-inline d-flex justify-content-between">
	                        <label>Email: </label><input type="text" id="email" name="email" placeholder="correo@sitio.com">
	                    </div>
	                    <div class="form-inline d-flex justify-content-between">
	                        <label>Sitio: </label><input type="text" id="site" name="site" placeholder="127.0.0.1">
	                    </div>
	                    <br>
	                    <div class="form-inline d-flex justify-content-between">
	                    	<a href="{{action('ClienteController@store')}}" class="btn btn-info">Volver</a>
	                    	<button type="submit" class="btn btn-primary">Guardar</button>
	                    </div>
                    </form>
                </div>
        	</div>
        </div>
	</x-slot>
</x-app-layout>