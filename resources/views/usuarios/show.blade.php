@php
    $empresa = DB::table('empresa_users')->where('id_user','=',Auth::user()->id)->select('id_empresa')->get();
    if ($empresa[0]->id_empresa != 1) {
        $sitio = DB::table('empresas')->where('id','=',$empresa[0]->id_empresa)->select('site')->get();
        header("Status: 200 Redireccion Programada");
        //dd("Location: ".$sitio[0]->site);
        header("Location: ".$sitio[0]->site);
        exit;
    }
@endphp
<x-app-layout>
	<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight d-flex justify-content-between">
            {{ __('Usuarios ') }}
            
        </h2>
    </x-slot>
    <div class="flex justify-end">
    	<a class="btn btn-primary" href="{{route('clientes.store')}}">Agregar</a>
    </div>
    <div class="flex justify-center">
        @if(count($users))
        	<div class="table-container">
        		<table class="table table-responsive table-striped rounded">
					<thead class="thead thead-dark rounded">
						<th>ID</th>
						<th>Nombre</th>
						<th>Email</th>
						<th>Ultimo Update</th>
						<th>Acciones</th>
					</thead>
					<tbody>
					@foreach($users as $user)
						<tr class="bg-warning text-white rounded">
							<td>{{$user->id}}</td>
							<td>{{$user->name}}</td>
							<td>{{$user->email}}</td>
							<td>{{$user->updated_at}}</td>
							<td>acciones</td>
						</tr>
					@endforeach
					</tbody>
				</table>
        	</div>
		@else
		NO HAY NAAADI!
		@endif
	</div>
</x-app-layout>