@php
    $empresa = DB::table('empresa_users')->where('id_user','=',Auth::user()->id)->select('id_empresa')->get();
    if ($empresa[0]->id_empresa != 1) {
        $sitio = DB::table('empresas')->where('id','=',$empresa[0]->id_empresa)->select('site')->get();
        header("Status: 200 Redireccion Programada");
        //dd("Location: ".$sitio[0]->site);
        header("Location: ".$sitio[0]->site);
        exit;
    }
@endphp
<x-app-layout>
	<x-slot name="header">
        <h2 class="d-flex font-semibold text-xl text-gray-800 leading-tight justify-content-between">
            {{ __('Clientes') }}
            <a class="btn btn-default" href="{{route('clientes.store')}}"><i class="fa fa-plus"></i></a>
        </h2>
    </x-slot>
    @if(count($empresas))
    	<div class="table-container table-responsive">
    		<table class="table table-striped rounded">
				<thead class="thead thead-dark rounded">
					<th>ID</th>
					<th>Nombre</th>
					<th>Rut</th>
					<th>Email</th>
					<th>Sitio</th>
					<th>Ultimo Update</th>
					<th>Acciones</th>
				</thead>
				<tbody>
				@foreach($empresas as $empresa)
					<tr class="bg-secondary rounded">
						<td>{{$empresa->id}}</td>
						<td>{{$empresa->nombre}}</td>
						<td>{{$empresa->rut}}</td>
						<td>{{$empresa->email}}</td>
						<td>{{$empresa->site}}</td>
						<td>{{$empresa->updated_at}}</td>
						<td class="form-inline">
							<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#create{{$empresa->id}}">
							  <i class="ni ni-single-02"></i>
							</button>
							<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#edit{{$empresa->id}}">
							  <i class="ni ni-settings"></i>
							</button>
							<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{$empresa->id}}">
							  <i class="fa fa-trash"></i>
							</button>
							<!-- Modal -->
						<div class="modal fade" id="create{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        <div class="panel">
				        			<form method="POST" action="{{route('users.store')}}">
				        				@csrf
					                    <div class="form-inline d-flex justify-content-between">
					                        <label>Nombre: </label><input type="text" id="nombre" name="nombre" placeholder="nombre">
					                    </div>
					                    <div class="form-inline d-flex justify-content-between">
					                        <label>Email: </label><input type="text" id="email" name="email" placeholder="correo@sitio.com">
					                    </div>
					                    <div class="form-inline d-flex justify-content-between">
					                        <label>Sitio: </label><input type="text" id="site" name="site" placeholder="127.0.0.1">
					                    </div>
					                    <br>
					                    <div class="form-inline d-flex justify-content-between">
					                    	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					                    	<button type="submit" class="btn btn-primary">Guardar</button>
					                    </div>
				                    </form>
				                </div>
						      </div>
						    </div>
						  </div>
						</div>
						<!-- Modal -->
						<div class="modal fade" id="edit{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Editar Empresas</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        <form method="POST" action="{{route('clientes.update',$empresa->id)}}">
			        				@csrf
			        				<input type="hidden" name="_method" value="PUT">
				                    <div class="form-inline d-flex justify-content-between">
				                        <label>Nombre: </label><input type="text" id="nombre" name="nombre" placeholder="nombre" value="{{$empresa->nombre}}">
				                    </div>
				                    <div class="form-inline d-flex justify-content-between">
				                        <label>RUT: </label><input type="text" id="rut" name="rut" placeholder="xx.xxx.xxx-x" value="{{$empresa->rut}}">
				                    </div>
				                    <div class="form-inline d-flex justify-content-between">
				                        <label>Email: </label><input type="text" id="email" name="email" placeholder="correo@sitio.com" value="{{$empresa->email}}">
				                    </div>
				                    <div class="form-inline d-flex justify-content-between">
				                        <label>Sitio: </label><input type="text" id="site" name="site" placeholder="127.0.0.1" value="{{$empresa->site}}">
				                    </div>
				                    <br>
				                    <div class="form-inline d-flex justify-content-between">
				                    	<a href="{{route('clientes.store')}}" class="btn btn-info">Volver</a>
				                    	<button type="submit" class="btn btn-primary">Guardar</button>
				                    </div>
			                    </form>
						      </div>
						    </div>
						  </div>
						</div>
						<!-- Modal -->
						<div class="modal fade" id="delete{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        ...
						      </div>
						    </div>
						  </div>
						</div>
						</td>
						
					</tr>
				@endforeach
				</tbody>
			</table>
    	</div>
	@else
	NO HAY NAAADI!
	@endif
</x-app-layout>
