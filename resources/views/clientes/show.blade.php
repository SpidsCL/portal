@php
    $empresa = DB::table('empresa_users')->where('id_user','=',Auth::user()->id)->select('id_empresa')->get();
    if ($empresa[0]->id_empresa != 1) {
        $sitio = DB::table('empresas')->where('id','=',$empresa[0]->id_empresa)->select('site')->get();
        header("Status: 200 Redireccion Programada");
        //dd("Location: ".$sitio[0]->site);
        header("Location: ".$sitio[0]->site);
        exit;
    }
@endphp
<x-app-layout>
	<x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Clientes') }}
            <a class="btn btn-primary" href="{{route('clientes.store')}}">Agregar</a>
        </h2>
        @if(count($usuarios))
        <div class="card">
        	<div class="card-body table-container">
        		<table class="table table-responsive table-striped rounded">
					<thead class="thead thead-dark rounded">
						<th>ID</th>
						<th>Nombre</th>
						<th>Email</th>
						<th>Ultimo Update</th>
						<th>Acciones</th>
					</thead>
					<tbody>
					@foreach($usuarios as $usuario)
						<tr class="bg-warning text-white rounded">
							<td>{{$usuario->id}}</td>
							<td>{{$usuario->name}}</td>
							<td>{{$usuario->email}}</td>
							<td>{{$usuario->updated_at}}</td>
							
						</tr>
					@endforeach
					</tbody>
				</table>
        	</div>
        </div>
		@else
		NO HAY NAAADI!
		@endif
	</x-slot>
</x-app-layout>