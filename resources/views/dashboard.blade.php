@php
    $empresa = DB::table('empresa_users')->where('id_user','=',Auth::user()->id)->select('id_empresa')->get();
    if ($empresa[0]->id_empresa != 1) {
        $sitio = DB::table('empresas')->where('id','=',$empresa[0]->id_empresa)->select('site')->get();
        header("Status: 301 Moved Permanently");
        header("Location: ".$sitio[0]->site);
        exit;
    }
@endphp
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-80 leading-tight">
            {{ __('Crear Usuario') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                @livewire('crear-usuarios')
            </div>
        </div>
    </div>
</x-app-layout>
