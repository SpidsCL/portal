<a {{ $attributes->merge(['class' => 'block px-4 py-2 text-sm leading-5 text-light hover:bg-gray-100 focus:outline-none focus:bg-white transition duration-150 ease-in-out']) }}>{{ $slot }}</a>
