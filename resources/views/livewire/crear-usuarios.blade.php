<div class="card-body">
    <form method="POST" action="{{ route('users.store') }}">
        @csrf

        <div>
            <x-jet-label value="{{ __('Nombre') }}" />
            <x-jet-input class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
        </div>

        <div class="mt-4">
            <x-jet-label value="{{ __('Email') }}" />
            <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
        </div>

        <div class="mt-4">
            <x-jet-label value="{{ __('Empresa') }}" />
            @if(count($empresas))
                <select class="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" name="id_empresa">
                    @foreach($empresas as $empresa)
                        <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                    @endforeach
                </select>
            @else
                <input type="text" name="id_empresa" disabled="true" value="1">
            @endif
        </div>

        <div class="mt-4">
            <x-jet-label value="{{ __('Contraseña') }}" />
            <x-jet-input class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
        </div>

        <div class="mt-4">
            <x-jet-label value="{{ __('Confirmar Contraseña') }}" />
            <x-jet-input class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
        </div>

        <div class="flex items-center justify-end mt-4">

            <x-jet-button class="ml-4">
                {{ __('Registrar') }}
            </x-jet-button>
        </div>
    </form>
</div>
