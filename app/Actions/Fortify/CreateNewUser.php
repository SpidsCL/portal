<?php

namespace App\Actions\Fortify;

use DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'id_empresa' => 'required'
        ])->validate();
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
        $empresa = DB::table('empresa_users')->insertGetId([
            'id_empresa'    => $data['id_empresa'],
            'id_user'       => $user->id,
            'created_at'    => now(),
            'updated_at'    => now()
        ]);
        if($user != null && $empresa != null){
            return $user;
        }else{
            return redirect()->back()->with('error','Registro no creado, verifique que los campos estén correctamente ingresados.');
        }
    }
}
