<?php

namespace App\Http\Livewire;

use App\models\Empresa;
use Livewire\Component;
use Illuminate\Support\Facades\Hash;
class CrearUsuarios extends Component
{
    public function render()
    {
    	$empresas = Empresa::all();
        return view('livewire.crear-usuarios',compact('empresas'));
    }

    
}
