<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use DB;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::all();
        return view('clientes.index',compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nombre' => ['required','string','max:255'],
            'rut'   => ['required','unique:empresas'],
            'email' => ['required','string'],
            'site'  => 'required'
        ]);
        $empresa = Empresa::create($request->all());
        $empresa->save();
        return redirect()->route('clientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuarios = 
            DB::table('users')
            ->leftJoin('empresa_users','users.id','=','empresa_users.id_user')
            ->where('empresa_users.id_empresa','=',$id)
            ->paginate(10);
        return view('clientes.show',compact('usuarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Empresa::find($id);
        return view('clientes.edit',compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nombre' => ['required','string','max:255'],
            'rut'   => ['required','unique:empresas'],
            'email' => ['required','string'],
            'site'  => ['required','url']
        ]);
        $empresa = Empresa::find($id);
        $empresa->fill($request->all());
        $empresa->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::find($id)->delete();
        return redirect()->back();
    }
}
